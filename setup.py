#!/usr/bin/env python

"""The setup script."""

from setuptools import find_packages, setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = ["Click~=7.0", "texttable"]

setup_requirements = ["pytest-runner", "pytest-mock"]

test_requirements = ["pytest>=3"]

setup(
    author="Shane Tzen",
    author_email="shane@tzen.cc",
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    description="mortgage costs estimator.",
    entry_points={"console_scripts": ["mortcalc=mortcalc.cli:main"]},
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="mortcalc",
    name="mortcalc",
    packages=find_packages(include=["mortcalc", "mortcalc.*"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/faultymonk/mortcalc",
    version="0.1.0",
    zip_safe=False,
)
