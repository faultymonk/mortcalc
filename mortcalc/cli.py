"""Console script for mortcalc."""
import sys

import click

from mortcalc.mortgage import Mortgage

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "-aca",
    "--annual-costs-additional",
    default=0,
    type=float,
    help="additional annual costs (default: 0)",
)
@click.option(
    "-ahi",
    "--annual-home-insurance",
    required=False,
    type=float,
    help="estimated annual home insurance (default: price * 0.15%)",
)
@click.option(
    "-apr",
    "--annual-percent-rate",
    default=4.25,
    type=float,
    help="annual interest rate (default: 4.25%)",
)
@click.option(
    "-ata",
    "--annual-taxes-additional",
    default=0,
    type=float,
    help="additional annual taxes (default: 0)",
)
@click.option(
    "-atr",
    "--annual-tax-rate",
    default=1.3,
    type=float,
    help="downpayment percentage (default: 1.3%)",
)
@click.option(
    "-emp",
    "--earnest-money-percent",
    default=3,
    type=float,
    help="earnest money deposit percent (default: 3%)",
)
@click.option(
    "-lt",
    "--loan-term",
    default=30,
    type=int,
    help="term of loan in years (default: 30)",
)
@click.option(
    "-pdp",
    "--percent-down-payment",
    default=20,
    type=float,
    help="downpayment percentage (default: 20%)",
)
@click.argument("purchase-price", required=True, type=int)
def main(
    annual_costs_additional: float,
    annual_taxes_additional: float,
    annual_home_insurance: float,
    annual_percent_rate: float,
    annual_tax_rate: float,
    earnest_money_percent: float,
    loan_term: int,
    percent_down_payment: float,
    purchase_price: int,
):
    """Console script for mortcalc."""
    mortgage = Mortgage(
        annual_costs_additional=annual_costs_additional,
        annual_taxes_additional=annual_taxes_additional,
        annual_home_insurance=annual_home_insurance,
        annual_percent_rate=annual_percent_rate,
        annual_tax_rate=annual_tax_rate,
        earnest_money_percent=earnest_money_percent,
        loan_term=loan_term,
        percent_down_payment=percent_down_payment,
        purchase_price=purchase_price,
    )

    click.echo(mortgage.pretty_output())
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
