import locale

from texttable import Texttable

supported_locales = list(locale.locale_alias.values())
if "en_US.utf8" in supported_locales:
    locale.setlocale(locale.LC_ALL, "en_US.utf8")
elif "en_US.UTF-8" in supported_locales:
    locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
elif "en_US.ISO8859" in supported_locales:
    locale.setlocale(locale.LC_ALL, "en_US.ISO8859")
else:
    locale.setlocale(locale.LC_ALL, "en_US")


class Mortgage:
    def __init__(
        self,
        annual_costs_additional: float,
        annual_percent_rate: float,
        annual_tax_rate: float,
        annual_taxes_additional: float,
        earnest_money_percent: float,
        loan_term: int,
        percent_down_payment: float,
        purchase_price: int,
        annual_home_insurance: float,
    ):
        self.annual_costs_additional = annual_costs_additional
        self.annual_percent_rate = annual_percent_rate
        self.annual_tax_rate = annual_tax_rate
        self.annual_taxes_additional = annual_taxes_additional
        self.earnest_money_percent = earnest_money_percent
        self.loan_term = loan_term
        self.percent_down_payment = percent_down_payment
        self.purchase_price = purchase_price
        self.annual_home_insurance = annual_home_insurance or (
            self.purchase_price * 0.001
        )

    @property
    def ad_valorem_tax(self) -> float:
        return self.purchase_price * self.annual_tax_rate / 100

    @property
    def annual_taxes_total(self) -> float:
        return self.ad_valorem_tax + self.annual_taxes_additional

    @property
    def down_payment(self) -> int:
        return self.purchase_price - self.principal

    @property
    def down_payment_balance(self) -> float:
        return self.down_payment - self.earnest_money

    @property
    def earnest_money(self) -> float:
        return self.purchase_price * self.earnest_money_percent / 100

    @property
    def principal(self) -> float:
        principal: float = self.purchase_price * (
            (100 - self.percent_down_payment)
        ) / 100
        return principal

    @property
    def annual_costs_total(self) -> float:
        return sum(
            [
                self.annual_costs_additional,
                self.annual_home_insurance,
                self.monthly_mortgage * 12,
                self.annual_taxes_total,
            ]
        )

    @property
    def monthly_taxes(self) -> float:
        return self.annual_taxes_total / 12

    @property
    def monthly_mortgage(self) -> float:
        return self.principal * (
            self.annual_percent_rate
            / (100 * 12)
            / (1 - (1 + self.annual_percent_rate / (100 * 12)) ** (-self.num_payments))
        )

    @property
    def monthly_total(self) -> float:
        return sum(
            [
                self.annual_costs_additional / 12,
                self.annual_home_insurance / 12,
                self.monthly_mortgage,
                self.monthly_taxes,
            ]
        )

    @property
    def num_payments(self) -> int:
        return self.loan_term * 12

    @property
    def total_cost(self) -> float:
        return self.down_payment + self.total_loan_cost

    @property
    def total_interest(self) -> float:
        return self.total_loan_cost - self.principal

    @property
    def total_loan_cost(self) -> float:
        return self.num_payments * self.monthly_mortgage

    def pretty_output(self):
        table = Texttable()
        table.set_cols_align(["l", "r"])
        table.set_deco(Texttable.VLINES | Texttable.BORDER | Texttable.HEADER)
        table.add_rows(
            [
                [
                    "Terms",
                    "{} years/{} @ {}% APR".format(
                        self.loan_term, self.num_payments, self.annual_percent_rate,
                    ),
                ],
                ["purchase price", locale.currency(self.purchase_price, "$", True)],
                ["loan amount", locale.currency(self.principal, "$", True)],
                [
                    f"down payment ({self.percent_down_payment}%)",
                    locale.currency(self.down_payment, "$", True),
                ],
                [
                    f"earnest money ({self.earnest_money_percent}%)",
                    locale.currency(self.earnest_money, "$", True),
                ],
                [
                    "down payment balance",
                    locale.currency(self.down_payment_balance, "$", True),
                ],
                ["total interest", locale.currency(self.total_interest, "$", True)],
                ["total loan cost", locale.currency(self.total_loan_cost, "$", True)],
                ["total cost", locale.currency(self.total_cost, "$", True)],
            ],
        )
        table.add_rows(
            [
                # annuals
                ["-" * 20, "-" * 20],
                ["Annual costs:", f"est. tax rate: {self.annual_tax_rate}%"],
                ["mortgage", locale.currency(self.monthly_mortgage * 12, "$", True)],
                ["ad valorem tax", locale.currency(self.ad_valorem_tax, "$", True)],
                [
                    "additional taxes",
                    locale.currency(self.annual_taxes_additional, "$", True),
                ],
                ["taxes, total", locale.currency(self.annual_taxes_total, "$", True)],
                [
                    "home insurance",
                    locale.currency(self.annual_home_insurance, "$", True),
                ],
                [
                    "costs, additional",
                    locale.currency(self.annual_costs_additional, "$", True),
                ],
                ["costs, total", locale.currency(self.annual_costs_total, "$", True)],
            ],
            header=False,
        )
        table.add_rows(
            [
                # monthlies
                ["-" * 20, "-" * 20],
                ["Monthly costs:", ""],
                ["mortgage", locale.currency(self.monthly_mortgage, "$", True)],
                ["property taxes", locale.currency(self.monthly_taxes, "$", True)],
                [
                    "home insurance",
                    locale.currency(self.annual_home_insurance / 12, "$", True),
                ],
                [
                    "costs, additional",
                    locale.currency(self.annual_costs_additional / 12, "$", True),
                ],
                ["costs, total", locale.currency(self.monthly_total, "$", True)],
            ],
            header=False,
        )
        return table.draw()
