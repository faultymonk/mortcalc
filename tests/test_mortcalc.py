#!/usr/bin/env python

"""Tests for `mortcalc` package."""

import pytest
from click.testing import CliRunner

from mortcalc import cli
from mortcalc.mortgage import Mortgage


@pytest.fixture
def mocked_mortgage(mocker):
    """mock class Mortgage"""
    patched = mocker.patch("mortcalc.cli.Mortgage", autospec=True)
    patched.return_value = 0
    return patched


def test_command_line_interface():
    """Test the CLI"""

    runner = CliRunner()

    help_result = runner.invoke(cli.main, ["-h"])
    assert help_result.exit_code == 0
    assert "Show this message and exit." in help_result.output

    result = runner.invoke(cli.main)
    assert 'Error: Missing argument "PURCHASE_PRICE"' in result.output

    result = runner.invoke(cli.main, "100000")
    assert result.exit_code == 0
    assert "purchase price       |              $100,000.00" in result.output


def test_default_parameters(mocked_mortgage):
    """Test defaults"""

    expected = dict(
        annual_costs_additional=0.0,
        annual_home_insurance=None,
        annual_percent_rate=4.25,
        annual_tax_rate=1.3,
        annual_taxes_additional=0.0,
        earnest_money_percent=3.0,
        loan_term=30,
        percent_down_payment=20.0,
        purchase_price=200000,
    )

    runner = CliRunner()
    runner.invoke(cli.main, "200000")

    mocked_mortgage.assert_called_once_with(**expected)


def test_expected_parameters(mocked_mortgage):
    """Test parameters are passed correctly"""

    args = [
        "--annual-costs-additional",
        "572",
        "--annual-home-insurance",
        "999",
        "--annual-percent-rate",
        "3.875",
        "--annual-tax-rate",
        "1.218",
        "--annual-taxes-additional",
        "2345",
        "--earnest-money-percent",
        "2.75",
        "--loan-term",
        "15",
        "--percent-down-payment",
        "18.5",
        "550000",
    ]
    expected = dict(
        annual_costs_additional=572.0,
        annual_home_insurance=999.0,
        annual_percent_rate=3.875,
        annual_tax_rate=1.218,
        annual_taxes_additional=2345.0,
        earnest_money_percent=2.75,
        loan_term=15,
        percent_down_payment=18.5,
        purchase_price=550000,
    )

    runner = CliRunner()
    runner.invoke(cli.main, args)

    mocked_mortgage.assert_called_once_with(**expected)


def test_mortgage_calculations():
    """Test various mortgage calculations"""
    m = Mortgage(
        annual_costs_additional=888.0,
        annual_home_insurance=777,
        annual_percent_rate=4.0,
        annual_tax_rate=1.1,
        annual_taxes_additional=555.0,
        earnest_money_percent=3.0,
        loan_term=30,
        percent_down_payment=15.0,
        purchase_price=654321,
    )

    assert round(m.ad_valorem_tax, 2) == 7197.53
    assert round(m.annual_costs_total, 2) == 41280.58
    assert round(m.annual_taxes_total, 2) == 7752.53
    assert round(m.down_payment_balance, 2) == 78518.52
    assert round(m.down_payment, 2) == 98148.15
    assert round(m.earnest_money, 2) == 19629.63
    assert round(m.monthly_mortgage, 2) == 2655.25
    assert round(m.monthly_taxes, 2) == 646.04
    assert round(m.monthly_total, 2) == 3440.05
    assert round(m.principal, 2) == 556172.85
    assert round(m.total_cost, 2) == 1054039.68
    assert round(m.total_interest, 2) == 399718.68
    assert round(m.total_loan_cost, 2) == 955891.53
